package org.simpllearn;

import java.util.Scanner;

public class Assignment05_1205 {
	
	public static void main(String[] args) {

		double plaweight = 0;
		Scanner s = new Scanner(System.in);
		System.out.print("Please enter your current weight:");
		double weight= s.nextDouble();
		System.out.println("\n"+"I have information for the following planets:"+"\n"+"  1.Venus   2.Mars    "
				+ "3.Jupiter"+"\n"+"  4.Saturn  5.Uranus  6.Neptune");
		System.out.print("\n"+"Which planet are you visiting?");
		int planum= s.nextInt();
		
		switch(planum) {
		case 1:
			plaweight = weight*0.78;
			System.out.println("\n"+"Your weight would be "+plaweight+" pounds on that planet");
			break;
		case 2:
			plaweight = weight*0.39;
			System.out.println("\n"+"Your weight would be "+plaweight+" pounds on that planet");
			break;
		case 3:
			plaweight = weight*2.65;
			System.out.println("\n"+"Your weight would be "+plaweight+" pounds on that planet");
			break;
		case 4:
			plaweight = weight*1.17;
			System.out.println("\n"+"Your weight would be "+plaweight+" pounds on that planet");
			break;
		case 5:
			plaweight = weight*1.05;
			System.out.println("\n"+"Your weight would be "+plaweight+" pounds on that planet");
			break;	
		case 6:
			plaweight = weight*1.23;
			System.out.println("\n"+"Your weight would be "+plaweight+" pounds on that planet");
			break;	
		
			default:
		System.out.println("\n"+"You are on earth, you would weigh the same");
			
		}
		
	}
}

